// 从日志中获取互助码

// process.env.SHARE_CODE_FILE = "/scripts/logs/sharecode.log";
// process.env.JD_COOKIE = "cookie1&cookie2";

exports.JDZZ_SHARECODES = [];
exports.DDFACTORY_SHARECODES = [];
exports.DREAM_FACTORY_SHARE_CODES = [];
exports.PLANT_BEAN_SHARECODES = [];
exports.FRUITSHARECODES = [];
exports.PETSHARECODES = [];
exports.JDJOY_SHARECODES = [];

let fileContent = '';
if (process.env.SHARE_CODE_FILE) {
    try {
        const fs = require('fs');
        if (fs.existsSync(process.env.SHARE_CODE_FILE)) fileContent = fs.readFileSync(process.env.SHARE_CODE_FILE, 'utf8');
    } catch (err) {
        console.error(err)
    }
}
let lines = fileContent.split('\n');

let shareCodesMap = {
    "JDZZ_SHARECODES": [],
    "DDFACTORY_SHARECODES": [],
    "DREAM_FACTORY_SHARE_CODES": [],
    "PLANT_BEAN_SHARECODES": [],
    "FRUITSHARECODES": [],
    "PETSHARECODES": [],
    "JDJOY_SHARECODES": [],
};
// for (let i = 0; i < lines.length; i++) {
//     if (lines[i].includes('京东赚赚')) {
//         shareCodesMap.JDZZ_SHARECODES.push(lines[i].split('】')[1].trim());
//     } else if (lines[i].includes('东东工厂')) {
//         shareCodesMap.DDFACTORY_SHARECODES.push(lines[i].split('】')[1].trim());
//     } else if (lines[i].includes('京喜工厂')) {
//         shareCodesMap.DREAM_FACTORY_SHARE_CODES.push(lines[i].split('】')[1].trim());
//     } else if (lines[i].includes('京东种豆得豆')) {
//         shareCodesMap.PLANT_BEAN_SHARECODES.push(lines[i].split('】')[1].trim());
//     } else if (lines[i].includes('东东农场')) {
//         shareCodesMap.FRUITSHARECODES.push(lines[i].split('】')[1].trim());
//     } else if (lines[i].includes('东东萌宠')) {
//         shareCodesMap.PETSHARECODES.push(lines[i].split('】')[1].trim());
//     } else if (lines[i].includes('crazyJoy')) {
//         shareCodesMap.JDJOY_SHARECODES.push(lines[i].split('】')[1].trim());
//     }
// }
shareCodesMap.JDZZ_SHARECODES.push("S5KkcR0wR91TTJkn1kaICIA");

//东东工厂
shareCodesMap.DDFACTORY_SHARECODES.push("T0225KkcR0wR91TTJkn1kaICIACjVWnYaS5kRrbA");
shareCodesMap.DDFACTORY_SHARECODES.push("T023a33MlYC0IMp6IRP8nfcDdnICjVWnYaS5kRrbA");
//shareCodesMap.DDFACTORY_SHARECODES.push("T0225KkcR0wR91TTJkn1kaICIACjVWnYaS5kRrbA");
shareCodesMap.DDFACTORY_SHARECODES.push("T01246woFVxCqgaCCjVWnYaS5kRrbA");

shareCodesMap.DREAM_FACTORY_SHARE_CODES.push("3YvsNYHCxVZ4eAfdUvIrmQ==");

//种豆得豆
shareCodesMap.PLANT_BEAN_SHARECODES.push("4npkonnsy7xi3mbdk36dqrla22wecrmgwvn4dai");
shareCodesMap.PLANT_BEAN_SHARECODES.push("kw45jfxad5hzuvfkie3nxcugs7vqf4qylkbrkti");
shareCodesMap.PLANT_BEAN_SHARECODES.push("nkvdrkoit5o64lidvo4sinyqsriciuiiw4wx3hq");
shareCodesMap.PLANT_BEAN_SHARECODES.push("kr3yixntjw5bg2lqx3tohyashm");

//东东农场
shareCodesMap.FRUITSHARECODES.push("9aae3ea7991c422b8585b6122912c3b5");
shareCodesMap.FRUITSHARECODES.push("db9ce87ef18348ec890cf37fd4e58634");
shareCodesMap.FRUITSHARECODES.push("20e67a0718e04853bfba7e024607d6ff");
shareCodesMap.FRUITSHARECODES.push("0e789b8e58ec465d90de19b17cecbafc");

//东东萌宠
shareCodesMap.PETSHARECODES.push("MTEyMTM1MTk3MDAwMDAwMDQ5NjQwNjQ3");
shareCodesMap.PETSHARECODES.push("MTEyMTY4MjgwMDAwMDAwNDk3NzQ5Njc=");
shareCodesMap.PETSHARECODES.push("MTE1MzEzNjI2MDAwMDAwMDUwMzQyMTQ3");
shareCodesMap.PETSHARECODES.push("MTEyNDExNjE0OTAwMDAwMDA0OTY0NzE2Nw==");

shareCodesMap.JDJOY_SHARECODES.push("yxSmgGyaoIYJQ3JQrNZap6t9zd5YaBeE");

for (let key in shareCodesMap) {
    shareCodesMap[key] = shareCodesMap[key].reduce((prev, cur) => prev.includes(cur) ? prev : [...prev, cur], []); // 去重
}
let cookieCount = 0;
if (process.env.JD_COOKIE) {
    if (process.env.JD_COOKIE.indexOf('&') > -1) {
        cookieCount = process.env.JD_COOKIE.split('&').length;
    } else {
        cookieCount = process.env.JD_COOKIE.split('\n').length;
    }
}

for (let key in shareCodesMap) {
    exports[key] = [];
    if (shareCodesMap[key].length === 0) {
        continue;
    }
    for (let i = 0; i < cookieCount; i++) {
        exports[key][i] = shareCodesMap[key].sort(() => Math.random() - 0.5).join('@');
    }
}

